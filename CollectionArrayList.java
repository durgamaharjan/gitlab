package cerotid.collection;

import java.util.ArrayList;
import java.util.Iterator;

public class CollectionArrayList {
	public static void main(String[] args) {
		//creating object of Arraylist
		ArrayList<Student> student=new ArrayList<>();
		
		//creating objects of Student class
		//id, first, middle, last, phone, email
		Student s1 = new Student(1111,"Christoper","Ashton","Kutcher",11111,"cakutcher@gmail.com");
		Student s2=new Student(2222,"William","Bradley","Pitt",222222,"wbpitt@gmail.com");
		Student s3=new Student(3333,"Laura","Jeanne","Witherspoon",33333,"ljwitherspoon@gmail.com");
		Student s4=new Student(4444,"Hannah","Dakota","Fanning",444444,"hdfanning@gmail.com");
		Student s5=new Student(5555,"John","William","Ferrell",555555,"jwferrell@gmail.com");
		Student s6=new Student(6666,"James","Paul","McCartney",66666,"jpmccartney@gmail.com");
		Student s7=new Student(7777,"Marvin","Neil","Simon",77777,"mnsimon@gmail.com");
		Student s8=new Student(8888,"Walter","Bruce","Willis",888888,"wbwillis@gmail.com");
		Student s9=new Student(9999,"Mary","Elle","Fanning",999999,"mefannning@gmail.com");
		Student s10=new Student(1010,"Robyn","Rihanna","Fenty",101010,"rrfenty@gmail.com");

		
		//adding Student's objects in Arraylist's object
		student.add(s1);
		student.add(s2);
		student.add(s3);
		student.add(s4);
		student.add(s5);
		student.add(s6);
		student.add(s7);
		student.add(s8);
		student.add(s9);
		student.add(s10);
		
		//using Iterator interface to iterator each object.
		
		Iterator<Student> itr=student.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		
	}
}
