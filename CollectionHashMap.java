package cerotid.collection;

import java.util.Iterator;
import java.util.HashMap;

public class CollectionHashMap {

	public static void main(String[] args) {
		HashMap<Integer, Student> studentMap = new HashMap<>();
	
		//creating Student's objects.
		Student s1=new Student(1111,"Christoper","Ashton","Kutcher",11111,"cakutcher@gmail.com");
		Student s2=new Student(2222,"William","Bradley","Pitt",222222,"wbpitt@gmail.com");
		Student s3=new Student(3333,"Laura","Jeanne","Witherspoon",33333,"ljwitherspoon@gmail.com");
		Student s4=new Student(4444,"Hannah","Dakota","Fanning",444444,"hdfanning@gmail.com");
		Student s5=new Student(5555,"John","William","Ferrell",555555,"jwferrell@gmail.com");
		Student s6=new Student(6666,"James","Paul","McCartney",66666,"jpmccartney@gmail.com");
		Student s7=new Student(7777,"Marvin","Neil","Simon",77777,"mnsimon@gmail.com");
		Student s8=new Student(8888,"Walter","Bruce","Willis",888888,"wbwillis@gmail.com");
		Student s9=new Student(9999,"Mary","Elle","Fanning",999999,"mefannning@gmail.com");
		Student s10=new Student(1010,"Robyn","Rihanna","Fenty",101010,"rrfenty@gmail.com");
		
		//listing all Student's objects in HashMap's object.
		studentMap.put(s1.getStudentID(), s1);
		studentMap.put(s2.getStudentID(), s2);
		studentMap.put(s3.getStudentID(), s3);
		studentMap.put(s4.getStudentID(), s4);
		studentMap.put(s5.getStudentID(), s5);
		studentMap.put(s6.getStudentID(), s6);
		studentMap.put(s7.getStudentID(), s7);
		studentMap.put(s8.getStudentID(), s8);
		studentMap.put(s9.getStudentID(), s9);
		studentMap.put(s10.getStudentID(), s10);
		
		//retrieving each objects using Iterator interface.
		Iterator keyIterator=studentMap.keySet().iterator();
		while(keyIterator.hasNext()) {
			Integer studentID=(Integer) keyIterator.next();
			Student student = (Student) studentMap.get(studentID);
			System.out.println("Student name: "+ student.getFirstName()+" "+ student.getMiddleName()+" "+student.getLastName());
			System.out.println("Student ID: "+ student.getStudentID());
			System.out.println("Phone No: "+student.getPhoneNo());
			System.out.println("Email: "+student.getEmail());
			System.out.println();
		}

	}
}
