package cerotid.collection;

public class Student {
	private int  studentID;
	private String firstName;
	private String middleName;
	private String lastName;
	private long  phoneNo;
	private String email;
	
	public Student(int studentID,String firstName,String middleName, String lastName, long phoneNo, String email) {
		this.studentID=studentID;
		this.firstName=firstName;
		this.middleName=middleName;
		this.lastName=lastName;
		this.phoneNo=phoneNo;
		this.email=email;
	}

	public int getStudentID() {
		return studentID;
	}

	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Student Name: " + firstName+" "+ middleName+" "+lastName+"\nStudent ID: "+studentID+" "+"\nPhone No: "+phoneNo+" "+ "\nEmail: "+email+"\n";
	}
	
	
}
